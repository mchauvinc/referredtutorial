'use strict';

deferredApp.controller('MainCtrl', function ($scope, playerService, $timeout, $http, $q) {
    // For visualization purposes
    $scope.messages = [];
    var player, videoId;

    var ytQ = playerService.create("ytplayer"); // Returns a deferred object

    // Called when the ytQ deferred object is resolved
    ytQ.promise.then(function (id) {
        $scope.messages.push("Player " + id + " has been loaded");
        player = document.getElementById(id); // set the player variable to the actual <object>
    }, function (id) {
        console.log("Well, that's weird");
    });

    // Scope, being a child of rootScope will receive the event
    $scope.$on("PLAYERLOADED", function (e, id) {
        ytQ.resolve(id); // resolve the object, ie state that it's done
        $scope.$apply(); // Missing this will result in the resolve being ignored
    });
    
    var httpPromise = $http.get('/').success(function (response) { // $http returns a promise object
        $scope.messages.push('Data has been loaded');
        videoId = "IRelx4-ISbs"; // Faking that the response includes the video id
    });

    // Combine the two promise objects*/
    $q.all([httpPromise, ytQ.promise]).then(function (results) {
        $scope.messages.push("Promise this will only happen after both data and player are loaded");
        // Let's load the video, now that we have both a player and the video id
        player.loadVideoById(videoId);
        $scope.messages.push("Starting video");
    });
});
