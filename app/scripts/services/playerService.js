'use strict';

deferredApp.factory('playerService', function ($window, $rootScope, $q) {
    $window.onYouTubePlayerReady = function (id) {
        $rootScope.$broadcast("PLAYERLOADED", id);
    }
    return {
        create: function (playerId) {
            var d = $q.defer();
            var params = { allowScriptAccess: "always" };
            var atts = { id: playerId };
            swfobject.embedSWF("http://www.youtube.com/apiplayer?enablejsapi=1&version=3&playerapiid="+playerId,
                               "ytapiplayer", "425", "356", "8", null, null, params, atts);
            return d;
        }
    }
});
